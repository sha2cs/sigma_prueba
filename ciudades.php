<?php

    $url = "assets/data.json";
    $json = file_get_contents($url);
    $obj = json_decode($json, true);

    $response = array();

    $departamento = $_POST['departamento'];
    $response['resultado'] = "<option value='@'>---</option>";

    foreach($obj[$departamento] AS $key => $value){
        $response['resultado'] .= "
            <option value='$value'>$value</option>
        ";
    }

    echo json_encode($response);
    exit();